package jmagine

import grails.converters.XML
import org.springframework.mock.web.MockMultipartFile

import java.util.regex.Pattern

class BootStrap {

    PoiService poiService
    ParcoursService parcoursService
    UserService userService

    def grailsApplication

    File[] deleteOldfFiles(File root, String regex){

            if(!root.isDirectory()) {
                throw new IllegalArgumentException(root+" is no directory.");
            }
            final Pattern p = Pattern.compile(regex);

             File[] listFiles = root.listFiles(new FileFilter(){
                @Override
                 boolean accept(File file) {
                    return p.matcher(file.getName()).matches();
                }
            });

        for (i in 0..< listFiles.size()) {
            listFiles[i].delete()

        }
    }


    def init = { servletContext ->


//        deleteOldfFiles(new File(grailsApplication.config.grails.assetspath.images), "(img)[0-9]*(.jpg)")

        XML.createNamedConfig('api_basic') { it.registerObjectMarshaller( new GenericMarshaller() ) }

        if( !Role.findByAuthority('ROLE_OP') ) {
            User userOp, userOp2, userAdmin, userMod


            def roleOp = new Role( authority: 'ROLE_OP', level:3 ).save(flush: false, failOnError: true )
            def roleAdmin = new Role( authority: 'ROLE_ADMIN', level:2 ).save(flush: false, failOnError: true )
            def roleModerator = new Role( authority: 'ROLE_MOD', level:1 ).save(flush: false, failOnError: true )


            userAdmin = userService
                    .createUser( username: "admin",
                            password: "password",
                            mail: "admin@tokidev.fr",
                            role:roleAdmin,
                            thumbnail: null )

            userOp2 = userService.createUser(imageType:'upload',
                    username: "lionel_tokidev2",
                    password: "password",
                    mail: "lionel@tokidev.fr",
                    role:roleOp,
                    thumbnail: null )
            try {
                FileInputStream user_op_fis =
                        new FileInputStream( new File( "src\\main\\webapp\\images\\user_01.jpg" ) );
                MockMultipartFile user_op_mmpf  = new MockMultipartFile( "test.txt", "test.png", "image/png", user_op_fis );

                userOp = userService.createUser(imageType:'upload',
                        username: "lionel_tokidev",
                        password: "password",
                        mail: "lionel@tokidev.fr",
                        role:roleOp,
                        thumbnail: user_op_mmpf )



                userMod = userService
                        .createUser( username: "modo",
                                password: "modo",
                                mail: "modo@tokidev.fr",
                                role:roleModerator,
                                thumbnail: null )

                (0..10).each {
                    userService
                            .createUser( username: "modo"+it,
                                    password: "modo",
                                    mail: "modo@tokidev"+it+".fr",
                                    role:roleModerator,
                                    thumbnail: null )
                }

            }
            catch(e) {
                e.printStackTrace()
                println "Erreur durant le chargement du fichier"
            }
            try {


                FileInputStream inputStream_parcours =
                        new FileInputStream(
                                new File("src\\main\\webapp\\images\\musee_arts_asiatiques.jpg"));

                FileInputStream inputStream_parcours2 =
                        new FileInputStream(
                                new File("src\\main\\webapp\\images\\musee_mamac.jpg"));

                FileInputStream inputStream_parcours3 =
                        new FileInputStream(
                                new File("src\\main\\webapp\\images\\musee_matisse.jpg"));

                FileInputStream inputStream_parcours4 =
                        new FileInputStream(
                                new File("src\\main\\webapp\\images\\musee_marc_chagall.jpg"));

                

                MockMultipartFile bg_parcours =
                        new MockMultipartFile("test.txt",
                                "test.png", "image/png", inputStream_parcours);

                 MockMultipartFile bg_parcours2 =
                        new MockMultipartFile("test.txt",
                                "test.png", "image/png", inputStream_parcours2);


                MockMultipartFile bg_parcours3 =
                        new MockMultipartFile("test.txt",
                                "test.png", "image/png", inputStream_parcours3);

                MockMultipartFile bg_parcours4 =
                        new MockMultipartFile("test.txt",
                                "test.png", "image/png", inputStream_parcours4);

                Parcours parcours = parcoursService
                        .createParcours(title: 'Musées Nice',
                                imageType: 'upload', background_picture: bg_parcours,
                                description: 'Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem', author: userOp, isValidated: true)

                Parcours parcours2 = parcoursService
                        .createParcours(title: 'Musées Mamac',
                                imageType: 'upload', background_picture: bg_parcours2,
                                description: 'Lorem Lorem Lorem Lorem Lorem ', author: userOp, isValidated: true)



                Parcours parcours3 = parcoursService
                        .createParcours(title: 'Musées Marc Chagall',
                                imageType: 'upload', background_picture: bg_parcours3,
                                description: 'Lorem Lorem Lorem Lorem Lorem Lorem Lorem ', author: userOp, isValidated: true)

                Parcours parcours4 = parcoursService
                        .createParcours(title: 'Musées Matisse',
                                imageType: 'upload', background_picture: bg_parcours4,
                                description: 'Lorem Lorem Lorem Lorem Lorem Lorem Lorem ', author: userOp, isValidated: true)

                parcoursService.addComponent(title: 'Composant', imageType: 'upload',
                        'background_picture': bg_parcours, content: '<b>Huehuehue</b>', parcours: parcours)

                FileInputStream inputStream1 =
                        new FileInputStream(new File("src\\main\\webapp\\images\\musee_arts_asiatiques.jpg"));
                MockMultipartFile bg_musee_arts_asiatiques =
                        new MockMultipartFile("test.txt", "test.png", "image/png", inputStream1);

                FileInputStream inputStream2 =
                        new FileInputStream(new File("src\\main\\webapp\\images\\musee_beaux_arts.jpg"));
                MockMultipartFile bg_musee_beaux_arts =
                        new MockMultipartFile("test.txt", "test.png", "image/png", inputStream2);

                FileInputStream inputStream3 = new FileInputStream(new File("src\\main\\webapp\\images\\musee_marc_chagall.jpg"));
                MockMultipartFile bg_marc_chagall = new MockMultipartFile("test.txt", "test.png", "image/png", inputStream3);

                FileInputStream inputStream4 = new FileInputStream(new File("src\\main\\webapp\\images\\musee_mamac.jpg"));
                MockMultipartFile bg_mamac = new MockMultipartFile("test.txt", "test.png", "image/png", inputStream4);

                FileInputStream inputStream5 = new FileInputStream(new File("src\\main\\webapp\\images\\musee_matisse.jpg"));
                MockMultipartFile bg_matisse = new MockMultipartFile("test.txt", "test.png", "image/png", inputStream5);

                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.668065', lng: '7.216075', title: 'Musée des Arts Asiatiques', address: '405 Promenade des Anglais, 06200 Nice', author: userOp, background_picture: bg_musee_arts_asiatiques)
                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.694539', lng: '7.248851', title: 'Musée des Beaux Arts de Nice', address: '33 Avenue des Baumettes, 06000 Nice', author: userOp, background_picture: bg_musee_beaux_arts)
                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.709137', lng: '7.269403', title: 'Musée National Marc Chagall', address: '36 Avenue Docteur Ménard, 06000 Nice', author: userOp, background_picture: bg_marc_chagall)
                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.70146', lng: '7.278485', title: 'Musée d\'Art Moderne et d\'Art Contemporain', address: 'Place Yves Klein, 06364 Nice', author: userOp, background_picture: bg_mamac)
                poiService.createPOI(imageType: 'upload', parcours: parcours, lat: '43.719536', lng: '7.275381', title: 'Musée Matisse', address: '164 Avenue des Arènes de Cimiez, 06000 Nice', author: userOp, background_picture: bg_matisse)
           
                poiService.createPOI(imageType: 'upload', parcours: parcours2, lat: '43.709137', lng: '7.269403', title: 'Musée National Marc Chagall', address: '36 Avenue Docteur Ménard, 06000 Nice', author: userOp, background_picture: bg_marc_chagall)
                poiService.createPOI(imageType: 'upload', parcours: parcours2, lat: '43.70146', lng: '7.278485', title: 'Musée d\'Art Moderne et d\'Art Contemporain', address: 'Place Yves Klein, 06364 Nice', author: userOp, background_picture: bg_mamac)
                poiService.createPOI(imageType: 'upload', parcours: parcours2, lat: '43.719536', lng: '7.275381', title: 'Musée Matisse', address: '164 Avenue des Arènes de Cimiez, 06000 Nice', author: userOp, background_picture: bg_matisse)

                poiService.createPOI(imageType: 'upload', parcours: parcours3, lat: '43.668065', lng: '7.216075', title: 'Musée des Arts Asiatiques', address: '405 Promenade des Anglais, 06200 Nice', author: userOp, background_picture: bg_musee_arts_asiatiques)
                poiService.createPOI(imageType: 'upload', parcours: parcours3, lat: '43.694539', lng: '7.248851', title: 'Musée des Beaux Arts de Nice', address: '33 Avenue des Baumettes, 06000 Nice', author: userOp, background_picture: bg_musee_beaux_arts)
                poiService.createPOI(imageType: 'upload', parcours: parcours3, lat: '43.709137', lng: '7.269403', title: 'Musée National Marc Chagall', address: '36 Avenue Docteur Ménard, 06000 Nice', author: userOp, background_picture: bg_marc_chagall)
                poiService.createPOI(imageType: 'upload', parcours: parcours3, lat: '43.719536', lng: '7.275381', title: 'Musée Matisse', address: '164 Avenue des Arènes de Cimiez, 06000 Nice', author: userOp, background_picture: bg_matisse)

                poiService.createPOI(imageType: 'upload', parcours: parcours4, lat: '43.694539', lng: '7.248851', title: 'Musée des Beaux Arts de Nice', address: '33 Avenue des Baumettes, 06000 Nice', author: userOp, background_picture: bg_musee_beaux_arts)
                poiService.createPOI(imageType: 'upload', parcours: parcours4, lat: '43.709137', lng: '7.269403', title: 'Musée National Marc Chagall', address: '36 Avenue Docteur Ménard, 06000 Nice', author: userOp, background_picture: bg_marc_chagall)
                poiService.createPOI(imageType: 'upload', parcours: parcours4, lat: '43.70146', lng: '7.278485', title: 'Musée d\'Art Moderne et d\'Art Contemporain', address: 'Place Yves Klein, 06364 Nice', author: userOp, background_picture: bg_mamac)


            }
            catch (e) {
                e.printStackTrace()
                println 'erreur creation pois/parcours'
            }
        }
    }


    def destroy = {

    }
}
